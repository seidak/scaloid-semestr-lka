﻿# Vzorovy projekt vyuzivajici Scaloid


Pro zprovozneni - IntelliJ IDEA (Android Studio)
			- Po otevreni projektu budou vyskakovat hlasky co je potreba nainstalovat - Scala plugin, Scala sdk

Pote jiz pujde aplikace zkompilovat a pustit na mobilnim zarizeni. 


Prerequisites according to Scaloid
-------------
* Android build tool 22.0.1
* Android SDK Level 16
 - Level 16 is required for building, while this app retains runtime compatibility from API Level 10. Please refer to `minSdkVersion` property in `build.gradle`
