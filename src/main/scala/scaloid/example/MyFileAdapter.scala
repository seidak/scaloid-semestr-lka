package scaloid.example

import java.io.{File, FilenameFilter}

import android.content.{Context, Intent}
import android.os.Environment
import android.view.View.OnClickListener
import android.view.{LayoutInflater, View, ViewGroup}
import android.widget.{BaseAdapter, ImageView, TextView}


/**
 * Created by Tadeas on 1. 6. 2015.
 *
 * Obecne adapteru na platforme Android znamenaji tridy, ktere dodavaji data pro listview.
 * Tzn kdyz se adapter inicializuje poskytne se mu nejaky data source a nasledne si listview rika,
 * at mu da konkretni view na pozici X. Adapter view vytvori (nebo view recykluje) a vrati spravne view, ktere nasledne
 * listview zobrazi na displeji (metoda getView)
 *
 * MyFileAdapter slouzi pro zobrazovani obsahu na externim ulozisti, pri kliknuti na konkretni view
 * si aktualizuje slozky a listview se aktualizuje (zobrazi se slozky a souboru rozkliknute slozky)
 *
 * Soubory jsou zobrazovane pouze s priponou .avi, ktere se po kliku prehraji v nove activite.
 */
class MyFileAdapter(val gContext: Context) extends BaseAdapter {
  private var fileArray: Array[File] = getFolderContent(Environment.getExternalStorageDirectory)
  private val inflater: LayoutInflater = LayoutInflater.from(gContext)

  override def getItemId(i: Int): Long = i

  override def getCount: Int = fileArray length

  override def getView(i: Int, view: View, viewGroup: ViewGroup): View = {
    val view = inflater.inflate(R.layout.listview_folder, viewGroup, false)
    val name = view.findViewById(R.id.name).asInstanceOf[TextView]
    val icon = view.findViewById(R.id.icon).asInstanceOf[ImageView]
    val file = fileArray(i)
    name setText (fileArray(i) getName)
    if (i == 0)
      name setText "../"
    if (!fileArray(i).isDirectory) {
      icon setImageResource R.drawable.ic_movie_film_icon_itunes_store
    }else{
      icon setImageResource R.drawable.ic_folder
    }
    view setOnClickListener new OnClickListener {
      override def onClick(view: View): Unit = {
        if (file isDirectory) {
          fileArray = getFolderContent(file)

          notifyDataSetChanged
        } else {
          /*operator new je u tridy intent (i jinde) vyuzivan, protoze puvodni android java objekty
            nemaji metody apply
           */
          val intent: Intent = new Intent(gContext, classOf[MediaPlayerActivity])
          intent.putExtra("path", file.getAbsolutePath)
          gContext startActivity intent
        }
      }
    }
    view
  }

  override def getItem(i: Int): AnyRef = fileArray(i)

  def getFoldersOnly(file: File): Array[File] = {
    file.listFiles(new FilenameFilter {
      override def accept(file: File, s: String): Boolean = new File(file, s).isDirectory
    }) sortWith (_.getName < _.getName)
  }

  def getMediaFilesOnly(file: File): Array[File] = {
    file.listFiles(new FilenameFilter {
      override def accept(file: File, name: String): Boolean = name endsWith ".avi"
    }) sortWith (_.getName < _.getName)
  }

  def getFolderContent(file: File): Array[File] = {
    Array(file getParentFile) ++ getFoldersOnly(file) ++ getMediaFilesOnly(file)
  }
}
