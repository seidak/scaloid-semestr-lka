package scaloid.example

import android.app.Activity
import android.media.MediaPlayer.{OnBufferingUpdateListener, OnCompletionListener, OnPreparedListener, OnVideoSizeChangedListener}
import android.media.{AudioManager, MediaPlayer}
import android.os.Bundle
import android.util.Log
import android.view.View.OnClickListener
import android.view.{SurfaceHolder, SurfaceView, View}

/**
 * Created by Tadeas on 1. 6. 2015.
 *
 * Tato aktivita je pouze prehravani videa a musi implementovat povinne metody jednotlivych listeneru.
 *
 */
class MediaPlayerActivity extends Activity with OnBufferingUpdateListener with OnCompletionListener with OnPreparedListener with OnVideoSizeChangedListener with SurfaceHolder.Callback {
  val TAG: String = "Media Player"
  val MEDIA: String = "media"
  val LOCAL_AUDIO: Integer = 1
  val STREAM_AUDIO: Integer = 2
  val RESOURCES_AUDIO: Integer = 3
  val LOCAL_VIDEO: Integer = 4
  val STREAM_VIDEO: Integer = 5


  /*tyto var jsou potrebne z duvodu, ze vsechny parametry se pri prehravani video mohou opakovane a libovolne menit*/

  private var mVideoWidth: Int = 0
  private var mVideoHeight: Int = 0
  private var mMediaPlayer: MediaPlayer = null
  private var mPreview: SurfaceView = null
  private var holder: SurfaceHolder = null
  private var path: String = null
  private var extras: Bundle = null
  private var mIsVideoSizeKnown: Boolean = false
  private var mIsVideoReadyToBePlayed: Boolean = false
  private var isPlaying: Boolean = false

  /**
   *
   * volani pri vytvareni activity
   */
  override def onCreate(icicle: Bundle) {
    super.onCreate(icicle)
    setContentView(R.layout.mediaplayer_2)
    mPreview = findViewById(R.id.surface).asInstanceOf[SurfaceView]
    mPreview.setOnClickListener(new OnClickListener {
      override def onClick(view: View): Unit = {
        if (isPlaying) {
          isPlaying = false
          mMediaPlayer pause
        } else {
          isPlaying = true
          mMediaPlayer start
        }
      }
    })
    holder = mPreview.getHolder
    holder.addCallback(this)
    holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
    extras = getIntent.getExtras
  }

  private def playVideo(gPath: String) {
    doCleanUp
    try {
      /*Media match {
        case LOCAL_VIDEO =>
          path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString + "/Naruto-Shippuden-414-CZ.avi"
          if (path eq "") {
            Toast.makeText(MediaPlayerActivity.this, "Please edit MediaPlayerDemo_Video Activity, " + "and set the path variable to your media file path." + " Your media file must be stored on sdcard.", Toast.LENGTH_LONG).show
          }
        case STREAM_VIDEO =>
          path = ""
          if (path eq "") {
            Toast.makeText(MediaPlayerActivity.this, "Please edit MediaPlayerDemo_Video Activity," + " and set the path variable to your media file URL.", Toast.LENGTH_LONG).show
          }
      }*/
      path = gPath

      mMediaPlayer = new MediaPlayer
      mMediaPlayer.setDataSource(path)
      mMediaPlayer.setDisplay(holder)
      mMediaPlayer.prepare
      mMediaPlayer.setOnBufferingUpdateListener(this)
      mMediaPlayer.setOnCompletionListener(this)
      mMediaPlayer.setOnPreparedListener(this)
      mMediaPlayer.setOnVideoSizeChangedListener(this)
      mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
    }
    catch {
      case e: Exception => {
        Log.e(TAG, "error: " + e.getMessage, e)
      }
    }
  }

  def onBufferingUpdate(arg0: MediaPlayer, percent: Int) {
    Log.d(TAG, "onBufferingUpdate percent:" + percent)
  }

  def onCompletion(arg0: MediaPlayer) {
    Log.d(TAG, "onCompletion called")
  }

  def onVideoSizeChanged(mp: MediaPlayer, width: Int, height: Int) {
    Log.v(TAG, "onVideoSizeChanged called")
    if (width == 0 || height == 0) {
      Log.e(TAG, "invalid video width(" + width + ") or height(" + height + ")")
      return
    }
    mIsVideoSizeKnown = true
    mVideoWidth = width
    mVideoHeight = height
    if (mIsVideoReadyToBePlayed && mIsVideoSizeKnown) {
      startVideoPlayback
    }
  }

  def onPrepared(mediaplayer: MediaPlayer) {
    Log.d(TAG, "onPrepared called")
    mIsVideoReadyToBePlayed = true
    if (mIsVideoReadyToBePlayed && mIsVideoSizeKnown) {
      startVideoPlayback
    }
  }

  def surfaceChanged(surfaceholder: SurfaceHolder, i: Int, j: Int, k: Int) {
    Log.d(TAG, "surfaceChanged called")
  }

  def surfaceDestroyed(surfaceholder: SurfaceHolder) {
    Log.d(TAG, "surfaceDestroyed called")
  }

  def surfaceCreated(holder: SurfaceHolder) {
    Log.d(TAG, "surfaceCreated called")
    playVideo(getIntent.getStringExtra("path"))
  }

  protected override def onPause {
    super.onPause
    releaseMediaPlayer
    doCleanUp
  }

  protected override def onDestroy {
    super.onDestroy
    releaseMediaPlayer
    doCleanUp
  }

  private def releaseMediaPlayer {
    if (mMediaPlayer != null) {
      mMediaPlayer.release
      mMediaPlayer = null
    }
  }

  private def doCleanUp {
    mVideoWidth = 0
    mVideoHeight = 0
    mIsVideoReadyToBePlayed = false
    mIsVideoSizeKnown = false
  }

  private def startVideoPlayback {
    Log.v(TAG, "startVideoPlayback")
    //holder.setFixedSize(mVideoWidth, mVideoHeight)
    mMediaPlayer.start
    isPlaying = true
  }


}