package scaloid.example

import org.scaloid.common._

/**
  * https://github.com/pocorall/scaloid - link na knihovnu
 *
 * HelloScaloid je startovaci aktivita, v mem pripade se do obsahu prida pouze listview, ktere zobrazuje obsahy slozek
 *
 *
  */
class HelloScaloid extends SActivity {

  /**
   *
   * Ukazkovy onCreate z samplu knihovny, v klasickem androidu se to vetsinou resi pridavani view pres XML soubory ve slozce res/layout
   *
   * onCreate {
        contentView = new SVerticalLayout {
          style {
            case b: SButton => b.textColor(Color.RED).onClick(toast("Bang!"))
            case t: STextView => t textSize 10.dip
            case e: SEditText => e.backgroundColor(Color.YELLOW).textColor(Color.BLACK)
          }
          STextView("I am 10 dip tall")
          STextView("Me too")
          STextView("I am 15 dip tall") textSize 15.dip // overriding
        this += new SLinearLayout {
          STextView("Button: ")
            SButton(R.string.red)
        }.wrap
        SEditText("Yellow input field fills the space").fill
        } padding 20.dip
      }
   */


  onCreate {
    val customAdapter = new MyFileAdapter(this)
    contentView = new SListView() adapter customAdapter
  }

}
